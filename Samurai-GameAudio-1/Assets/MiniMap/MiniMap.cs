using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    public Transform player;
    public Transform playerCamera;

    public Transform MapMemory1;
    public Transform MapMemory2;

    bool goToMemory1;
    bool goToMemory2;

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string miniMapAudio;
    public FMOD.Studio.PARAMETER_ID parameterMiniMapZoomInId;
    public FMOD.Studio.PARAMETER_ID parameterMiniMapZoomOutId;
    string zoomInParam = "ZoomIn";
    string zoomOutParam = "ZoomOut";


    void Awake()
    {
        transform.position = MapMemory1.position;
        goToMemory1 = true;
    }

    void Start()
    {
        FMOD.Studio.EventDescription miniMapEventDescription = FMODUnity.RuntimeManager.GetEventDescription(miniMapAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION miniMapParameterDescriptionZoomIn;
        miniMapEventDescription.getParameterDescriptionByName(zoomInParam, out miniMapParameterDescriptionZoomIn);
        parameterMiniMapZoomInId = miniMapParameterDescriptionZoomIn.id;

        FMOD.Studio.PARAMETER_DESCRIPTION miniMapParameterDescriptionZoomOut;
        miniMapEventDescription.getParameterDescriptionByName(zoomOutParam, out miniMapParameterDescriptionZoomOut);
        parameterMiniMapZoomOutId = miniMapParameterDescriptionZoomOut.id;
    }

    void Update()
    {
        MoveCam();
        MoveConditions();
    }

    void PlayMiniMapAudio(float zoomInVal, float zoomOutVal)
    {
        FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(miniMapAudio);
        oneShot.setParameterByID(parameterMiniMapZoomInId, zoomInVal);
        oneShot.setParameterByID(parameterMiniMapZoomOutId, zoomOutVal);
        oneShot.start();
        oneShot.release();
    }

    void LateUpdate()
    {
        Vector3 newPosition = player.position;
        newPosition.y = transform.position.y;
        transform.position = newPosition;

        transform.rotation = Quaternion.Euler(90f, playerCamera.eulerAngles.y, 0f);
    }

    void MoveCam()
    {
        if (goToMemory1)
        {
            transform.position = Vector3.Lerp(transform.position, MapMemory1.position, Time.deltaTime);
        }
        else if (goToMemory2)
        {
            transform.position = Vector3.Lerp(transform.position, MapMemory2.position, Time.deltaTime);
        }
    }

    void MoveConditions()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            if (goToMemory2)
                PlayMiniMapAudio(1, 0);
            
            goToMemory1 = true;
            goToMemory2 = false;
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            if (goToMemory1)
                PlayMiniMapAudio(0, 1);

            goToMemory2 = true;
            goToMemory1 = false;
        }
    }
}
