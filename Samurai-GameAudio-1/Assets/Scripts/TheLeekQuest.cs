using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TheLeekQuest : MonoBehaviour
{
    [Header("Triggers")]
    public GameObject leekTrigger;
    public GameObject marketSellerTrigger;
    GameObject marketLady;

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string questCompleteAudio;
    [FMODUnity.EventRef]
    public string leekPickupAudio;
    public FMOD.Studio.PARAMETER_ID parameterLeekPickupId;
    string leekPickupParam = "LeekPickup";
    [FMODUnity.EventRef]
    public string marketLadyAudio = "";
    FMOD.Studio.EventInstance marketLadyEventInstance;
    public FMOD.Studio.PARAMETER_ID parameterLeekId;
    string leekStateParam = "LeekDialogue";

    [Header("UI Elements")]
    public GameObject pressToTalkUI;
    public GameObject pressToPickUpUI;
    public GameObject pressToGiveUI;
    public GameObject leekUIImage;
    public GameObject subtitleUI;
    GameObject gongQuest;

    [Header("Items")]
    public GameObject leekToCollect;
    public GameObject leekToGive;
    public bool questStarted;
    public bool questCompleted = false;
    public bool doesPlayerHaveLeek;
    bool hasPlayerGivenLeek;

    // Start is called before the first frame update
    void Start()
    {
        questStarted = false;
        hasPlayerGivenLeek = false;
        doesPlayerHaveLeek = false;

        pressToGiveUI.SetActive(false);
        pressToPickUpUI.SetActive(false);
        pressToTalkUI.SetActive(false);
        leekToGive.SetActive(false);
        leekUIImage.SetActive(false);
        gongQuest = GameObject.Find("----- Gong Quest -----");
        
        marketLady = GameObject.Find("MarketSellerAudio");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(marketLadyEventInstance, marketLady.transform, marketLady.GetComponent<Rigidbody>());

        FMOD.Studio.EventDescription marketLadyEventDescription = FMODUnity.RuntimeManager.GetEventDescription(marketLadyAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION marketLadyParameterDescription;
        marketLadyEventDescription.getParameterDescriptionByName(leekStateParam, out marketLadyParameterDescription);
        parameterLeekId = marketLadyParameterDescription.id;

        FMOD.Studio.EventDescription leekEventDescription = FMODUnity.RuntimeManager.GetEventDescription(leekPickupAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION leekParameterDescription;
        leekEventDescription.getParameterDescriptionByName(leekPickupParam, out leekParameterDescription);
        parameterLeekPickupId = leekParameterDescription.id;
    }

    // Update is called once per frame
    void Update()
    {
        SubtitleUpdate();
        SellerSpeech();
        LeekPickup();
        MarketSellerEnd();
        MarketSellerStart();
    }

    void SubtitleUpdate()
    {
        if (questStarted && marketSellerTrigger.GetComponent<MarketSellerTrigger>().playerIsInMarketSellerTrigger == false && !gongQuest.GetComponent<GongQuest>().questStarted)
        {
            subtitleUI.SetActive(false);
        }
    }

    void PlaySpeech(float newParamterValue)
    {
        FMOD.Studio.EventInstance marketOneShot = FMODUnity.RuntimeManager.CreateInstance(marketLadyAudio);
        marketOneShot.setParameterByID(parameterLeekId, newParamterValue);
        marketOneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(marketLady));
        marketOneShot.start();
        marketOneShot.release();
    }

    void PlayQuestSound(string audio)
    {
        FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(audio);
        oneShot.start();
        oneShot.release();
    }

    void PlayLeekPickUp(float val)
    {
        FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(leekPickupAudio);
        oneShot.setParameterByID(parameterLeekPickupId, val);
        oneShot.start();
        oneShot.release();
    }


    void MarketSellerStart()
    {
        if (marketSellerTrigger.GetComponent<MarketSellerTrigger>().playerIsInMarketSellerTrigger == true && questStarted == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("Start Leek Quest");

                PlaySpeech(0f);
                subtitleUI.GetComponent<Text>().text = "Could I ask a favour? There are leeks that have been harvested but I cannot leave my market stall to collect them. Would you be so kind as to bring them to me?";
                subtitleUI.SetActive(true);
          
                questStarted = true;
                pressToTalkUI.SetActive(false);
            }
        }
    }

    void SellerSpeech()
    {
        if (marketSellerTrigger.GetComponent<MarketSellerTrigger>().playerIsInMarketSellerTrigger == true && questStarted == true && doesPlayerHaveLeek == false && hasPlayerGivenLeek == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlaySpeech(1f);
                Debug.Log("No Leeks Found");
                pressToTalkUI.SetActive(false);
            }
        }
        else if (marketSellerTrigger.GetComponent<MarketSellerTrigger>().playerIsInMarketSellerTrigger == true && questStarted == true && hasPlayerGivenLeek == true)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlaySpeech(4f);
                Debug.Log("After Leek Quest");
                pressToTalkUI.SetActive(false);
            }
        }
    }

    void LeekPickup()
    {
        if (questStarted == true && leekTrigger.GetComponent<LeekPickup>().playerIsInLeekTrigger == true && doesPlayerHaveLeek == false)
        {
            pressToPickUpUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayLeekPickUp(1);
                pressToPickUpUI.SetActive(false);
                Destroy(leekToCollect);
                doesPlayerHaveLeek = true;
                leekUIImage.SetActive(true);
            }
        }
    }

    void MarketSellerEnd()
    {
        if (hasPlayerGivenLeek == false && doesPlayerHaveLeek == true && marketSellerTrigger.GetComponent<MarketSellerTrigger>().playerIsInMarketSellerTrigger == true)
        {
            pressToTalkUI.SetActive(false);
            pressToGiveUI.SetActive(true); //CHANGE KEY TO GIVE TO G OR SUMMIT TO AVOID CONFUSION 
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlaySpeech(3f);
                PlayQuestSound(questCompleteAudio);
                PlayLeekPickUp(0);
                subtitleUI.GetComponent<Text>().text = "Thank you so much, my customers will be so relieved!";
                subtitleUI.SetActive(true);
                Debug.Log("Leek Quest Complete");

                questCompleted = true;
                hasPlayerGivenLeek = true;
                pressToGiveUI.SetActive(false);
                leekToGive.SetActive(true);
                leekUIImage.SetActive(false);
            }
        }
    }
}
