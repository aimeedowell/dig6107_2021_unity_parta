using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaryGuard : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        inTrigger = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (inTrigger && !hasAlreadyTriggeredAudio)
        {
            guardAudioEvent.SendMessage("Play");
            hasAlreadyTriggeredAudio = true;
        }
    }

    private void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            inTrigger = true;
        }
    }

    private void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            hasAlreadyTriggeredAudio = false;
            inTrigger = false;
        }
    }

        [Header("FMOD")]
    public GameObject guardAudioEvent;
    bool inTrigger = false;
    bool hasAlreadyTriggeredAudio = false;
}
