using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GongQuest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        questStarted = false;
        questComplete = false;
        hasPlayerHitGong = false;

        subtitleUI.SetActive(false);
        pressToTalkUI.SetActive(false);
        gongUIImage.SetActive(false);
        countdownTextUI.SetActive(false);
        TimeToDisplay(timeRemaining);

        leekQuest = GameObject.Find("----- Veg Quest -----");
        swordQuest = GameObject.Find("QuestLogic");

        guardGongObject = GameObject.Find("GuardAudio(Gong)");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(guardGongAudioEventInstance, guardGongObject.transform, guardGongObject.GetComponent<Rigidbody>());

        FMOD.Studio.EventDescription marketLadyEventDescription = FMODUnity.RuntimeManager.GetEventDescription(guardGongAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION marketLadyParameterDescription;
        marketLadyEventDescription.getParameterDescriptionByName(guardGongSpeechParam, out marketLadyParameterDescription);
        parameterGuardGongId = marketLadyParameterDescription.id;
    }

    // Update is called once per frame
    void Update()
    {
        SubtitleUpdate();
        Countdown();
        GuardSpeech();
        GongQuestStart();
        HitGong();
    }

    void Countdown()
    {
        if (!hasTimeRanOut && questStarted)
        {
            if (timeRemaining > 0)
            {
                countdownTextUI.SetActive(true);
                timeRemaining -= Time.deltaTime;
                TimeToDisplay(timeRemaining);
                if (timeRemaining <= 30)
                {
                    countdownTextUI.GetComponent<Text>().color = Color.red;
                }
            }
            else
            {
                hasTimeRanOut = true;
                questFailed = true;
                timeRemaining = 0f;
                countdownTextUI.SetActive(false);
            }
        }
    }

    void SubtitleUpdate()
    {
        if (questStarted && swordQuest.GetComponent<QuestLogic>().swordQuestStarted == false && gongStartTrigger.GetComponent<GongStartTrigger>().playerInGongStartQuestTrigger == false)
        {
            subtitleUI.SetActive(false);
        }
    }

    void TimeToDisplay(float time)
    {
        time += 1; //convert time to seconds only (no milliseconds)
        float minutes = Mathf.FloorToInt(time / 60);
        float seconds = Mathf.FloorToInt(time % 60);
        countdownText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        countdownTextUI.GetComponent<Text>().text = countdownText.text;
        
    }

    void PlaySpeech(float newParamterValue)
    {
        FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(guardGongAudio);
        oneShot.setParameterByID(parameterGuardGongId, newParamterValue);
        oneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(guardGongObject));
        oneShot.start();
        oneShot.release();
    }

    void PlayQuestSound(string audio)
    {
        FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(audio);
        oneShot.start();
        oneShot.release();
    }

    void GuardSpeech()
    {
        if (gongStartTrigger.GetComponent<GongStartTrigger>().playerInGongStartQuestTrigger == true && questStarted == true && !hasPlayerHitGong && !hasTimeRanOut)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlaySpeech(1f);
                Debug.Log("You don't have much time left... Please try and hit the gong!");
                pressToTalkUI.SetActive(false);
            }
        }
        else if (gongStartTrigger.GetComponent<GongStartTrigger>().playerInGongStartQuestTrigger == true && questStarted == true && hasPlayerHitGong == true && questFailed == false && swordQuest.GetComponent<QuestLogic>().swordQuestStarted == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlaySpeech(3f);
                PlayQuestSound(questCompleteAudio);
                subtitleUI.GetComponent<Text>().text = "You did it! I heard it loud and clear. On behalf of the villagers, thank you so much!";
                subtitleUI.SetActive(true);
                questComplete = true;
                Debug.Log("Gong Quest Success");
                gongUIImage.SetActive(false);
                pressToTalkUI.SetActive(false);
            }
        }
        else if (gongStartTrigger.GetComponent<GongStartTrigger>().playerInGongStartQuestTrigger == true && questStarted == true && questFailed == true && swordQuest.GetComponent<QuestLogic>().swordQuestStarted == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlaySpeech(2f);
                subtitleUI.GetComponent<Text>().text = "It's gone 12 and I still haven't heard the gong... Nevermind, there's always 1 o'clock...";
                subtitleUI.SetActive(true);
                questComplete = true;
                Debug.Log("Gong Quest Failed");
                pressToTalkUI.SetActive(false);
            }
        }
    }

    void HitGong()
    {
        if (questStarted == true && gongHitTrigger.GetComponent<HitGong>().playerInHitGongTrigger == true && hasPlayerHitGong == false)
        {
            hasPlayerHitGong = true;
            if (timeRemaining > 0)
            {
                gongUIImage.SetActive(true);
                timeRemaining = 0f;
                hasTimeRanOut = true;
                countdownTextUI.SetActive(false);
                questFailed = false;
            }
            else
            {
                questFailed = true;
            }
        }
    }

    void GongQuestStart()
    {
        if (leekQuest.GetComponent<TheLeekQuest>().questCompleted == true && gongStartTrigger.GetComponent<GongStartTrigger>().playerInGongStartQuestTrigger == true && questStarted == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlaySpeech(0f);
                subtitleUI.GetComponent<Text>().text = "It's nearly 12 o'clock. That means the village gong needs to be hit. I have to stay here to guard the church. Please go and hit the gong for me. You only have 2 minutes!";
                subtitleUI.SetActive(true);
                Debug.Log("Start Gong Quest");

                questStarted = true;
                pressToTalkUI.SetActive(false);
            }
        }
    }

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string questCompleteAudio;
    [FMODUnity.EventRef]
    public string guardGongAudio = "";
    FMOD.Studio.EventInstance guardGongAudioEventInstance;
    public FMOD.Studio.PARAMETER_ID parameterGuardGongId;
    string guardGongSpeechParam = "GongDialogue";

    [Header("Triggers")]
    public GameObject gongHitTrigger;
    public GameObject gongStartTrigger;
    GameObject guardGongObject;

    [Header("UI Elements")]
    public GameObject pressToTalkUI;
    public GameObject countdownTextUI;
    public GameObject subtitleUI;
    public GameObject gongUIImage;
    GameObject leekQuest;
    GameObject swordQuest;

    bool hasPlayerHitGong;
    public bool questStarted;
    public bool questComplete;
    public bool questFailed = true;

    [Header("Countdown Timer")]
    public bool hasTimeRanOut = false;
    float timeRemaining = 120;
    public Text countdownText;
}
