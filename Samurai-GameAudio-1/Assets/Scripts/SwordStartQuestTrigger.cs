using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordStartQuestTrigger : MonoBehaviour
{
    public bool isPlayerInSwordQuestStartTrigger;
    // Start is called before the first frame update
    void Start()
    {
        isPlayerInSwordQuestStartTrigger = false;
        music = GameObject.Find("ThirdPersonController");
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            isPlayerInSwordQuestStartTrigger = true;
            float val = music.GetComponent<MusicControl>().currentMusicVolume;
            music.GetComponent<MusicControl>().ChangeParameter("GlobalMusicVol", (val - 0.3f));
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            isPlayerInSwordQuestStartTrigger = false;
            float val = music.GetComponent<MusicControl>().currentMusicVolume;
            music.GetComponent<MusicControl>().ChangeParameter("GlobalMusicVol", val);
        }
    }

    GameObject music;
}
