using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitGong : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        playerInHitGongTrigger = false;
    }

    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerInHitGongTrigger = true;
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerInHitGongTrigger = false;
        }
    }

    public bool playerInHitGongTrigger = false;

}
