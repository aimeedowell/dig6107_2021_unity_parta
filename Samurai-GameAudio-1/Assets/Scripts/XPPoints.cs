using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XPPoints : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        leekQuest = GameObject.Find("----- Veg Quest -----");
        swordQuest = GameObject.Find("QuestLogic");
        gongQuest = GameObject.Find("----- Gong Quest -----");
        pointsAchievedMessage.GetComponent<Text>().text = "";
    }

    // Update is called once per frame
    void Update()
    {
        Text text = pointsAchievedMessage.GetComponent<Text>();

        if (leekQuest.GetComponent<TheLeekQuest>().questCompleted && !leekUIDone)
        {
            points += 100f;
            pointsAchievedMessage.GetComponent<Text>().text = "+ 100 XP \n Leek Quest Complete";
            text.CrossFadeAlpha(1.0f, 0f, false); //fade in
            text.CrossFadeAlpha(0.0f, 2.5f, false); //fade out
            pointsAchievedMessage.SetActive(true);
            leekUIDone = true;
        }
        else if (gongQuest.GetComponent<GongQuest>().questComplete && !gongQuest.GetComponent<GongQuest>().questFailed && !gongUIDone)
        {
            points += 100f;
            pointsAchievedMessage.GetComponent<Text>().text = "+ 100 XP \n Gong Quest Complete";
            text.CrossFadeAlpha(1.0f, 0f, false); //fade in
            text.CrossFadeAlpha(0.0f, 2.5f, false); //fade out
            pointsAchievedMessage.SetActive(true);
            gongUIDone = true;
        }
        else if (swordQuest.GetComponent<QuestLogic>().questComplete && !swordUIDone)
        {
            points += 100f;
            pointsAchievedMessage.GetComponent<Text>().text = "+ 100 XP \n Gong Quest Complete";
            text.CrossFadeAlpha(1.0f, 0f, false); //fade in
            text.CrossFadeAlpha(0.0f, 2.5f, false); //fade out
            levelComplete = true;
            pointsAchievedMessage.SetActive(true);
            swordUIDone = true;
        }
        else if (templeArea.GetComponent<NewAreaDiscovered>().hasBeenTriggered && !templeUIDone)
        {
            points += 50f;
            pointsAchievedMessage.GetComponent<Text>().text = "+ 50 XP \n New Area Discovered";
            text.CrossFadeAlpha(1.0f, 0f, false); //fade in
            text.CrossFadeAlpha(0.0f, 2.5f, false); //fade out
            pointsAchievedMessage.SetActive(true);
            templeUIDone = true;
        }
        else if (zenArea.GetComponent<NewAreaDiscovered>().hasBeenTriggered && !zenUIDone)
        {
            points += 50f;
            pointsAchievedMessage.GetComponent<Text>().text = "+ 50 XP \n New Area Discovered";
            text.CrossFadeAlpha(1.0f, 0f, false); //fade in
            text.CrossFadeAlpha(0.0f, 2.5f, false); //fade out
            pointsAchievedMessage.SetActive(true);
            zenUIDone = true;
        }
        else if (hillArea.GetComponent<NewAreaDiscovered>().hasBeenTriggered && !hillUIDone)
        {
            points += 50f;
            pointsAchievedMessage.GetComponent<Text>().text = "+ 50 XP \n New Area Discovered";
            text.CrossFadeAlpha(1.0f, 0f, false); //fade in
            text.CrossFadeAlpha(0.0f, 2.5f, false); //fade out
            pointsAchievedMessage.SetActive(true);
            hillUIDone = true;
        }
        pointTally.GetComponent<Text>().text = points.ToString() + " XP";
    }

    public void DisablePointTally()
    {
        Text text = pointTally.GetComponent<Text>();
        text.CrossFadeAlpha(0.0f, 2.5f, false); //fade out
    }


    [Header("UI Elements")]
    public GameObject pointsAchievedMessage;
    public GameObject pointTally;
    public GameObject templeArea;
    public GameObject zenArea;
    public GameObject hillArea;
    GameObject gongQuest;
    GameObject swordQuest;
    GameObject leekQuest;

    public float points = 0f;
    public bool levelComplete = false;
    bool leekUIDone = false;
    bool gongUIDone = false;
    bool swordUIDone = false;
    bool templeUIDone = false;
    bool zenUIDone = false;
    bool hillUIDone = false;
}
