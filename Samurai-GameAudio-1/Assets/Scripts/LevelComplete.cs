using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelComplete : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        XPObject = GameObject.Find("XPPoints");
        player = GameObject.Find("Camera");

        Text text = levelCompleteText.GetComponent<Text>();
        text.CrossFadeAlpha(0f, 0f, false);
        levelCompleteBackground.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (pointTally.GetComponent<XPPoints>().levelComplete)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                points = XPObject.GetComponent<XPPoints>().points;
                XPObject.GetComponent<XPPoints>().DisablePointTally();
                levelCompleteText.GetComponent<Text>().text = "LEVEL COMPLETE! \n \n" + points + " XP";
                Text text = levelCompleteText.GetComponent<Text>();
                text.CrossFadeAlpha(1f, 4f, false);
                levelCompleteBackground.SetActive(true);
                player.GetComponent<Camera>().enabled = false;
                pointTallyText.SetActive(false);
                playSound = true;
            }
        }
        PlayCompleteSound();
    }

    void PlayCompleteSound()
    {
        if (playSound && !hasPlayed)
        {
            FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(levelCompleteAudio);
            oneShot.start();
            oneShot.release();
            hasPlayed = true;
            FMODUnity.RuntimeManager.StudioSystem.setParameterByName("MasterBusVolume", 1f);
        }
    }

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string levelCompleteAudio;

    [Header("UI Elements")]
    public GameObject pointTally;
    public GameObject pointTallyText;
    public GameObject levelCompleteText;
    public GameObject levelCompleteBackground;
    GameObject XPObject;
    GameObject player;

    bool playSound = false;
    bool hasPlayed = false;
    float timer = 2;

    float points = 0;
}
