using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueLeekQuestStarted : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        leekQuest = GameObject.Find("----- Veg Quest -----");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(dialogueEventInstance, dialogueTrigger.transform, dialogueTrigger.GetComponent<Rigidbody>());
    }

    // Update is called once per frame
    void Update()
    {
        PlaySpeech();
    }

    void PlaySpeech()
    {
        if (canPlayEvent && leekQuest.GetComponent<TheLeekQuest>().questStarted && !leekQuest.GetComponent<TheLeekQuest>().doesPlayerHaveLeek && !leekQuest.GetComponent<TheLeekQuest>().questCompleted)
        {
            FMOD.Studio.EventInstance dialogueOneShot = FMODUnity.RuntimeManager.CreateInstance(dialogueEvent);
            dialogueOneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(dialogueTrigger));
            dialogueOneShot.start();
            dialogueOneShot.release();
            canPlayEvent = false;
        }
    }


    private void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            canPlayEvent = true;
        }
    }

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string dialogueEvent = "";
    FMOD.Studio.EventInstance dialogueEventInstance;

    [Header("Trigger")]
    public GameObject dialogueTrigger;
    GameObject leekQuest;

    private bool canPlayEvent = false;
}
