using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GongStartTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        playerInGongStartQuestTrigger = false;
        music = GameObject.Find("ThirdPersonController");
    }

    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerInGongStartQuestTrigger = true;
            float val = music.GetComponent<MusicControl>().currentMusicVolume;
            music.GetComponent<MusicControl>().ChangeParameter("GlobalMusicVol", (val - 0.3f));
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerInGongStartQuestTrigger = false;
            float val = music.GetComponent<MusicControl>().currentMusicVolume;
            music.GetComponent<MusicControl>().ChangeParameter("GlobalMusicVol", val);
        }
    }

    public bool playerInGongStartQuestTrigger = false;
    GameObject music;
}
