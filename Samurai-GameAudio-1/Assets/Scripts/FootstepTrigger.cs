using System.Collections;
using System.Collections.Generic;
using System; //Array
using UnityEngine;

public class FootstepTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        playerController = GameObject.Find("ThirdPersonController");
        playerRigidBody = playerController.GetComponent<Rigidbody>();

        FMODUnity.RuntimeManager.AttachInstanceToGameObject(breathEventInstance, playerController.transform, playerRigidBody);
        FMOD.Studio.EventDescription eventDescription = FMODUnity.RuntimeManager.GetEventDescription(breathAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION parameterDescription;
        eventDescription.getParameterDescriptionByName(breathParam, out parameterDescription);
        breathParameterId = parameterDescription.id;

        FMODUnity.RuntimeManager.AttachInstanceToGameObject(footstepRunEventInstance, runFootstepObject.transform, runFootstepObject.GetComponent<Rigidbody>());
        FMOD.Studio.EventDescription runEventDescription = FMODUnity.RuntimeManager.GetEventDescription(footstepRunAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION runParameterDescription;
        runEventDescription.getParameterDescriptionByName(footstepRunParam, out runParameterDescription);
        footstepRunParameterId = runParameterDescription.id;

        FMODUnity.RuntimeManager.AttachInstanceToGameObject(footstepWalkEventInstance, walkFootstepObject.transform, walkFootstepObject.GetComponent<Rigidbody>());
        FMOD.Studio.EventDescription walkEventDescription = FMODUnity.RuntimeManager.GetEventDescription(footstepWalkAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION walkParameterDescription;
        walkEventDescription.getParameterDescriptionByName(footstepWalkParam, out walkParameterDescription);
        footstepWalkParameterId = walkParameterDescription.id;

        FMODUnity.RuntimeManager.AttachInstanceToGameObject(footstepJumpEventInstance, jumpFootstepObject.transform, jumpFootstepObject.GetComponent<Rigidbody>());
        FMOD.Studio.EventDescription jumpEventDescription = FMODUnity.RuntimeManager.GetEventDescription(footstepJumpAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION jumpParameterDescription;
        jumpEventDescription.getParameterDescriptionByName(footstepJumpParam, out jumpParameterDescription);
        footstepJumpParameterId = jumpParameterDescription.id;
    }

    // Update is called once per frame
    void Update()
    { 
        FootstepReleaseTimer();
        FindFootstepSurface();
    }

    float GetPlayerVelocity()
    {
        velocity.x = Mathf.Abs(playerRigidBody.velocity.x);
        velocity.z = Mathf.Abs(playerRigidBody.velocity.z);
        return (velocity.x + velocity.z);
    }

    void PlayRunFootstep()
    {
        if (!footstepRunActive)
        {
            if (GetPlayerVelocity() > movementGate)
            {
                FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(footstepRunAudio);
                oneShot.setParameterByID(footstepRunParameterId, currentSurfaceIndex);
                oneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(runFootstepObject));
                oneShot.start();
                oneShot.release();
                footstepRunActive = true;
            }
        }
    }

    void PlayWalkFootstep()
    {
        if (!footstepRunActive && !footstepWalkActive)
        {
            if (GetPlayerVelocity() > 0.1f)
            {
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.C))
                {
                    FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(footstepWalkAudio);
                    oneShot.setParameterByID(footstepWalkParameterId, currentSurfaceIndex);
                    oneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(walkFootstepObject));
                    oneShot.start();
                    oneShot.release();
                }
            }
        }
    }

    void PlayJumpFootstep()
    {
        if (playerController.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>().landedFromJump)
        {
            FindFootstepSurface();
            FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(footstepJumpAudio);
            oneShot.setParameterByID(footstepJumpParameterId, currentSurfaceIndex);
            oneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(jumpFootstepObject));
            oneShot.start();
            oneShot.release();
            playerController.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>().PlayJumpGrunt(1f);
            playerController.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>().landedFromJump = false;
        }
    }

    void PlayRunBreath()
    {
        FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(breathAudio);

        if (prevBreath == 1f)
        {
            oneShot.setParameterByID(breathParameterId, 0f);
            prevBreath = 0f;
        }
        else
        {
            oneShot.setParameterByID(breathParameterId, 1f);
            prevBreath = 1f;
        }

        oneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(playerController.transform));
        oneShot.start();
        oneShot.release();

    }

    void FootstepReleaseTimer()
    {
        if (footstepRunActive)
        {
            timer += (2 * Time.deltaTime) + resetModifier;

            if (timer > resetThresholdRun)
            {
                timer = 0f;
                footstepRunActive = false;
            }
        }
    }

    void FindFootstepSurface()
    {
        RaycastHit rayCast;

        if (Physics.Raycast(transform.position, Vector3.down, out rayCast))
        {
            surfaceTag = rayCast.collider.gameObject.tag;

            if (surfaceTag == "Untagged")
                currentSurfaceIndex = 3f;
        }

        foreach (string i in surfaceType)
        {
            if (surfaceTag == i)
            {
                float surfaceIdx = Array.IndexOf(surfaceType, i);
                currentSurfaceIndex = surfaceIdx;
            }
        }
    }

    GameObject playerController;
    Rigidbody playerRigidBody;
    Vector3 velocity;

    float timer = 0f;
    bool footstepRunActive = false;
    bool footstepWalkActive = false;
    string surfaceTag;

    string[] surfaceType = { "Grass", "Gravel", "Sand", "Tile", "Water", "Wood", "Bog" };

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string breathAudio = "";
    FMOD.Studio.EventInstance breathEventInstance;
    public FMOD.Studio.PARAMETER_ID breathParameterId;
    string breathParam = "JumpBreath";
    float prevBreath = 1f;
    [FMODUnity.EventRef]
    public string footstepRunAudio = "";
    FMOD.Studio.EventInstance footstepRunEventInstance;
    public FMOD.Studio.PARAMETER_ID footstepRunParameterId;
    string footstepRunParam = "SurfaceTypeRun";
    [FMODUnity.EventRef]
    public string footstepWalkAudio = "";
    FMOD.Studio.EventInstance footstepWalkEventInstance;
    public FMOD.Studio.PARAMETER_ID footstepWalkParameterId;
    string footstepWalkParam = "SurfaceTypeWalk";
    [FMODUnity.EventRef]
    public string footstepJumpAudio = "";
    FMOD.Studio.EventInstance footstepJumpEventInstance;
    public FMOD.Studio.PARAMETER_ID footstepJumpParameterId;
    string footstepJumpParam = "SurfaceTypeJump";

    [Header("FootstepGameObjects")]
    public GameObject runFootstepObject;
    public GameObject walkFootstepObject;
    public GameObject jumpFootstepObject;

    [Header("Footstep Gate")]
    public float resetModifier = 1f;
    public float resetThresholdRun = 2f;
    public float resetThresholdWalk = 0f;
    public float movementGate = 0.3f;

    float currentSurfaceIndex = 3f;
}
