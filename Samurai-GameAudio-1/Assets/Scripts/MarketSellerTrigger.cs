using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketSellerTrigger : MonoBehaviour
{

    public bool playerIsInMarketSellerTrigger;
    // Start is called before the first frame update
    void Start()
    {
        playerIsInMarketSellerTrigger = false;
        music = GameObject.Find("ThirdPersonController");
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerIsInMarketSellerTrigger = true;
            float val = music.GetComponent<MusicControl>().currentMusicVolume;
            music.GetComponent<MusicControl>().ChangeParameter("GlobalMusicVol", (val - 0.3f));
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerIsInMarketSellerTrigger = false;
            float val = music.GetComponent<MusicControl>().currentMusicVolume;
            music.GetComponent<MusicControl>().ChangeParameter("GlobalMusicVol", val);
        }
    }

    GameObject music;
}
