using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndQuestTrigger : MonoBehaviour
{

    public bool playerInEndTrigger;
    GameObject music;

    // Start is called before the first frame update
    void Start()
    {
        playerInEndTrigger = false;
        music = GameObject.Find("ThirdPersonController");
    }

    void Update()
    {
        //Debug status of trigger
        //Debug.Log("EndTrigger Status ="+playerInEndTrigger);
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider player)
    {
        if(player.gameObject.tag == "Player")
        {
            playerInEndTrigger = true;
            float val = music.GetComponent<MusicControl>().currentMusicVolume;
            music.GetComponent<MusicControl>().ChangeParameter("GlobalMusicVol", (val - 0.3f));
        } 
    }

    void OnTriggerExit(Collider player)
    {
        if(player.gameObject.tag == "Player")
        {
            playerInEndTrigger = false;
            float val = music.GetComponent<MusicControl>().currentMusicVolume;
            music.GetComponent<MusicControl>().ChangeParameter("GlobalMusicVol", val);
        }
    }
 
}
