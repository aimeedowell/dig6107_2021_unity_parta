using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewAreaDiscovered : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlaySound();
    }

    void PlaySound()
    {
        if (inTrigger && !hasBeenTriggered)
        {
            FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(newAreaAudio);
            oneShot.start();
            oneShot.release();
            hasBeenTriggered = true;
        }
    }

    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            inTrigger = true;
        }
    }

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string newAreaAudio;

    public bool hasBeenTriggered = false;
    public bool inTrigger = false;
}
