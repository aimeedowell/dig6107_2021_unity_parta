using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BambooCollision : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        playerObject = GameObject.Find("ThirdPersonController");
        playerPosition = playerObject.transform;
        rigidBody = playerObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        GetPlayerVelocity();
    }

    void GetPlayerVelocity()
    {
        zMove.z = Mathf.Abs(rigidBody.velocity.z);
        xMove.x = Mathf.Abs(rigidBody.velocity.x);

        playerVelocity = zMove.z + xMove.x;
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (playerVelocity > 0.5f)
            {
                playbackSpeed += (playerVelocity * Time.deltaTime);
                if (playbackSpeed > playbackThreshold)
                {
                    FMODUnity.RuntimeManager.PlayOneShot(rustleEvent, playerPosition.position);
                    playbackSpeed = 0f;
                }
            }
        }
    }

    GameObject playerObject;
    Rigidbody rigidBody;
    Transform playerPosition;

    float playerVelocity = 0f;
    public float playbackSpeed = 0f;
    public float playbackThreshold = 1f;

    [Header("FMOD")]

    [FMODUnity.EventRef]
    public string rustleEvent = "";

    Vector3 xMove;
    Vector3 zMove;
}
