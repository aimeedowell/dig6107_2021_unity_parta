using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmperorAudio : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        emperorObject = GameObject.Find("EmperorAudio");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(emperorAudioEventInstance, emperorObject.transform, emperorObject.GetComponent<Rigidbody>());
        FMOD.Studio.EventDescription emperorEventDescription = FMODUnity.RuntimeManager.GetEventDescription(emperorAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION emperorParameterDescription;
        emperorEventDescription.getParameterDescriptionByName(emperorSpeechParam, out emperorParameterDescription);
        parameterEmperorId = emperorParameterDescription.id;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [FMODUnity.EventRef]
    public string emperorAudio = "";
    FMOD.Studio.EventInstance emperorAudioEventInstance;
    public FMOD.Studio.PARAMETER_ID parameterEmperorId;
    string emperorSpeechParam = "EmperorDialogue";

    [Header("GameObjects")]
    GameObject emperorObject;
}
