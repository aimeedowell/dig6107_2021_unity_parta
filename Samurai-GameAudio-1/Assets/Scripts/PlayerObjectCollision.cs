using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObjectCollision : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        playerObject = GameObject.Find("ThirdPersonController");
        playerPosition = playerObject.transform;
        rigidBody = playerObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        GetPlayerVelocity();
        ReleaseTimer();
    }

    void ReleaseTimer()
    {
        if (hasTriggered)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                hasTriggered = false;
                timer = 2f;
            }
        }
    }

    void GetPlayerVelocity()
    {
        zMove.z = Mathf.Abs(rigidBody.velocity.z);
        xMove.x = Mathf.Abs(rigidBody.velocity.x);

        playerVelocity = (zMove.z + xMove.x);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.C)) //use GetKey to find out if button continously pressed
            {
                hasTriggered = true;
                return;
            }
            if (playerVelocity >= velocityThreshold && !hasTriggered)
            {
                FMODUnity.RuntimeManager.PlayOneShot(gruntAudioEvent, playerPosition.position);
                hasTriggered = true;
            }
        }
    }


    GameObject playerObject;
    Rigidbody rigidBody;
    Transform playerPosition;

    float velocityThreshold = 1.8f;
    float playerVelocity = 0f;
    float timer = 2f;
    bool hasTriggered = false;

    [Header("FMOD")]

    [FMODUnity.EventRef]
    public string gruntAudioEvent = "";

    Vector3 xMove;
    Vector3 zMove;
}
