using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamperDialogue : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { 
        leekQuest = GameObject.Find("----- Veg Quest -----");
        swordQuest = GameObject.Find("QuestLogic");
        gongQuest = GameObject.Find("----- Gong Quest -----");

        FMODUnity.RuntimeManager.AttachInstanceToGameObject(camperEventInstance, camperTrigger.transform, camperTrigger.GetComponent<Rigidbody>());
        FMOD.Studio.EventDescription emperorEventDescription = FMODUnity.RuntimeManager.GetEventDescription(camperAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION emperorParameterDescription;
        emperorEventDescription.getParameterDescriptionByName(camperParam, out emperorParameterDescription);
        camperParameterId = emperorParameterDescription.id;
    }

    // Update is called once per frame
    void Update()
    {
        QuestState();
    }

    void QuestState()
    {
        if (leekQuest.GetComponent<TheLeekQuest>().questStarted && !leekQuest.GetComponent<TheLeekQuest>().questCompleted)
        {
            PlaySpeech(1f);
        }
        else if (gongQuest.GetComponent<GongQuest>().questStarted && !gongQuest.GetComponent<GongQuest>().questComplete)
        {
            PlaySpeech(2f);
        }
        else if (swordQuest.GetComponent<QuestLogic>().swordQuestStarted && !swordQuest.GetComponent<QuestLogic>().questComplete)
        {
            PlaySpeech(3f);
        }
        else
        {
            PlaySpeech(0f);
        }
    }

    void PlaySpeech(float newParamterValue)
    {
        if (canPlayEvent)
        {
            FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(camperAudio);
            oneShot.setParameterByID(camperParameterId, newParamterValue);
            oneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(camperTrigger));
            oneShot.start();
            oneShot.release();
            canPlayEvent = false;
        }
    }

    private void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            canPlayEvent = true;
        }
    }

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string camperAudio = "";
    FMOD.Studio.EventInstance camperEventInstance;
    public FMOD.Studio.PARAMETER_ID camperParameterId;
    string camperParam = "Camper";

    [Header("Trigger")]
    public GameObject camperTrigger;
    GameObject leekQuest;
    GameObject swordQuest;
    GameObject gongQuest;


    private bool canPlayEvent = false;
}
