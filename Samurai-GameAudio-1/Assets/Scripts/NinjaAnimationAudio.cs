using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaAnimationAudio : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ninjaPunching = GameObject.Find("NinjaPunchSpeech");
        ninaSitUp = GameObject.Find("NinjaSitUpSpeech");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(punchEventInstance, ninjaPunching.transform, ninjaPunching.GetComponent<Rigidbody>());
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(sitUpEventInstance, ninaSitUp.transform, ninaSitUp.GetComponent<Rigidbody>());
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(sitUpBreatheEventInstance, ninaSitUp.transform, ninaSitUp.GetComponent<Rigidbody>());

    }

    // Update is called once per frame
    void Update()
    {

    }

    void PlayPunchEvent()
    {
        FMOD.Studio.EventInstance punchOneShot = FMODUnity.RuntimeManager.CreateInstance(punchEvent);
        punchOneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(ninjaPunching));
        punchOneShot.start();
        punchOneShot.release();
    }

    void PlaySitUpEvent()
    {
        FMOD.Studio.EventInstance sitUpOneShot = FMODUnity.RuntimeManager.CreateInstance(sitUpEvent);
        sitUpOneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(ninaSitUp));
        sitUpOneShot.start();
        sitUpOneShot.release();
    }

    void PlaySitUpBreatheEvent()
    {
        FMOD.Studio.EventInstance sitUpOneShot = FMODUnity.RuntimeManager.CreateInstance(sitUpBreatheEvent);
        sitUpOneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(ninaSitUp));
        sitUpOneShot.start();
        sitUpOneShot.release();
    }

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string sitUpEvent = "";
    [FMODUnity.EventRef]
    public string sitUpBreatheEvent = "";
    [FMODUnity.EventRef]
    public string punchEvent = "";
    FMOD.Studio.EventInstance sitUpEventInstance;
    FMOD.Studio.EventInstance sitUpBreatheEventInstance;
    FMOD.Studio.EventInstance punchEventInstance;

    [Header("GameObjects")]
    GameObject ninjaPunching;
    GameObject ninaSitUp;
}
