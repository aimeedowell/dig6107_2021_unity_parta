using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestLogic : MonoBehaviour
{
    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string questCompleteAudio;
    [FMODUnity.EventRef]
    public string swordPickupAudio;
    [FMODUnity.EventRef]
    public string guardAudio = "";
    FMOD.Studio.EventInstance guardAudioEventInstance;
    public FMOD.Studio.PARAMETER_ID parameterGuardId;
    string guardSpeechParam = "SwordDialogue";

    [FMODUnity.EventRef]
    public string emperorAudio = "";
    FMOD.Studio.EventInstance emperorAudioEventInstance;
    public FMOD.Studio.PARAMETER_ID parameterEmperorId;
    string emperorSpeechParam = "EmperorDialogue";

    [Header("Triggers")]
    public GameObject emperorsSwordEnd;
    public GameObject pickupTrigger;
    public GameObject startSwordQuestTrigger;
    public GameObject endquestTrigger;
    GameObject guardObject;
    GameObject emperorObject;

    [Header("UI Elements")]
    public GameObject pressToTalkUI;
    public GameObject pressToTalkUI2;
    public GameObject pickupUI;
    public GameObject giveUI;
    public GameObject dontHaveSwordUI;
    public GameObject pickupInventory;
    public GameObject subtitleUI;
    GameObject gongQuest;

    [Header("Items")]
    public GameObject emperorsSword;
    public bool swordQuestStarted = false;
    public bool questComplete;

    public bool playerHasEnteredSwordTrigger;
    public bool playerHasSword;
    public bool hasPlayerGivenSword = false;
    bool isEmperorTalking = false;

    // Start is called before the first frame update

    void Start()
    {
        playerHasSword = false;
        subtitleUI.SetActive(false);
        pressToTalkUI.SetActive(false);
        pressToTalkUI2.SetActive(false);
        pickupUI.SetActive(false);
        giveUI.SetActive(false);
        dontHaveSwordUI.SetActive(false);
        pickupInventory.SetActive(false);
        questComplete = false;
        emperorsSwordEnd.SetActive(false);

        gongQuest = GameObject.Find("----- Gong Quest -----");

        guardObject = GameObject.Find("GuardAudio");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(guardAudioEventInstance, guardObject.transform, guardObject.GetComponent<Rigidbody>());
        FMOD.Studio.EventDescription guardEventDescription = FMODUnity.RuntimeManager.GetEventDescription(guardAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION guardParameterDescription;
        guardEventDescription.getParameterDescriptionByName(guardSpeechParam, out guardParameterDescription);
        parameterGuardId = guardParameterDescription.id;

        emperorObject = GameObject.Find("EmperorAudio");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(emperorAudioEventInstance, emperorObject.transform, emperorObject.GetComponent<Rigidbody>());
        FMOD.Studio.EventDescription emperorEventDescription = FMODUnity.RuntimeManager.GetEventDescription(emperorAudio);
        FMOD.Studio.PARAMETER_DESCRIPTION emperorParameterDescription;
        emperorEventDescription.getParameterDescriptionByName(emperorSpeechParam, out emperorParameterDescription);
        parameterEmperorId = emperorParameterDescription.id;

    }

    // Update is called once per frame
    void Update()
    {
        SubtitleUpdate();
        SwordQuestSpeech();
        SwortQuestStart();
        PickupSword();
        EndQuest();
        DisplayUI();
    }

    void SubtitleUpdate()
    {
        if (swordQuestStarted == true && startSwordQuestTrigger.GetComponent<SwordStartQuestTrigger>().isPlayerInSwordQuestStartTrigger == false && !isEmperorTalking)
        {
            subtitleUI.SetActive(false);
        }
        else if (swordQuestStarted == true && endquestTrigger.GetComponent<EndQuestTrigger>().playerInEndTrigger == false && isEmperorTalking)
        {
            subtitleUI.SetActive(false);
            isEmperorTalking = false;
        }
    }

    void PlayGuardSpeech(float newParamterValue)
    {
        FMOD.Studio.EventInstance guardOneShot = FMODUnity.RuntimeManager.CreateInstance(guardAudio);
        guardOneShot.setParameterByID(parameterGuardId, newParamterValue);
        guardOneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(guardObject));
        guardOneShot.start();
        guardOneShot.release();
    }

    void PlayEmperorSpeech(float newParamterValue)
    {
        FMOD.Studio.EventInstance emperorOneShot = FMODUnity.RuntimeManager.CreateInstance(emperorAudio);
        emperorOneShot.setParameterByID(parameterEmperorId, newParamterValue);
        emperorOneShot.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(emperorObject));
        emperorOneShot.start();
        emperorOneShot.release();
    }

    void PlayQuestSound(string audio)
    {
        FMOD.Studio.EventInstance oneShot = FMODUnity.RuntimeManager.CreateInstance(audio);
        oneShot.start();
        oneShot.release();
    }

    void SwortQuestStart()
    {
        if (gongQuest.GetComponent<GongQuest>().questComplete == true && startSwordQuestTrigger.GetComponent<SwordStartQuestTrigger>().isPlayerInSwordQuestStartTrigger == true && swordQuestStarted == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayGuardSpeech(0f);
                subtitleUI.GetComponent<Text>().text = "I've lost the Emperor's sword.. If I don't find it soon I might lose my job. Please could you look for it?";
                subtitleUI.SetActive(true);
                Debug.Log("Start Quest");
                swordQuestStarted = true;
                pressToTalkUI.SetActive(false);
            }
        }
    }

    void SwordQuestSpeech()
    {
        if (startSwordQuestTrigger.GetComponent<SwordStartQuestTrigger>().isPlayerInSwordQuestStartTrigger == true && swordQuestStarted == true && playerHasSword == false && hasPlayerGivenSword == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayGuardSpeech(1f);
                Debug.Log("Sword Not Found");
                pressToTalkUI.SetActive(false);
            }
        }
        else if (startSwordQuestTrigger.GetComponent<SwordStartQuestTrigger>().isPlayerInSwordQuestStartTrigger == true && swordQuestStarted == true && playerHasSword == true && hasPlayerGivenSword == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayGuardSpeech(2f);
                subtitleUI.GetComponent<Text>().text = "You found the sword! Please give it to the Emperor, he'll be so pleased... and hurry!";
                subtitleUI.SetActive(true);
                Debug.Log("Sword Found Not Given");
                pressToTalkUI.SetActive(false);
            }
        }
        else if (startSwordQuestTrigger.GetComponent<SwordStartQuestTrigger>().isPlayerInSwordQuestStartTrigger == true && hasPlayerGivenSword == true)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayGuardSpeech(4f);
                Debug.Log("After Quest");
                questComplete = true;
                PlayQuestSound(questCompleteAudio);
                pressToTalkUI.SetActive(false);
            }
        }
        else if (endquestTrigger.GetComponent<EndQuestTrigger>().playerInEndTrigger == true && swordQuestStarted == true && playerHasSword == false && hasPlayerGivenSword == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayEmperorSpeech(0f);
                Debug.Log("Sword Not Found, Emperor");
                pressToTalkUI.SetActive(false);
            }
        }
    }

    void PickupSword()
    {
        if (swordQuestStarted == true && pickupTrigger.GetComponent<SwordPickup>().playerCanPickupSword == true && Input.GetKeyDown(KeyCode.E))
        {
            Destroy(emperorsSword);
            PlayQuestSound(swordPickupAudio);
            playerHasSword = true;
            pickupUI.SetActive(false);
            pickupInventory.SetActive(true);
        }

    }
    void EndQuest()
    {
        if (endquestTrigger.GetComponent<EndQuestTrigger>().playerInEndTrigger == true && playerHasSword && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Quest Ended, Emperor");
            PlayEmperorSpeech(1f);
            PlayQuestSound(swordPickupAudio);
            subtitleUI.GetComponent<Text>().text = "Huh! You found my sword! Keep up the good work and you'll make a great warrior one day...";
            hasPlayerGivenSword = true;
            subtitleUI.SetActive(true);
            isEmperorTalking = true;
            emperorsSwordEnd.SetActive(true);
            pickupUI.SetActive(false);
            playerHasSword = false;
            pickupInventory.SetActive(false);
        }
    }
    void DisplayUI()
    {
        //UI for Pickup
        if (swordQuestStarted == true && pickupTrigger.GetComponent<SwordPickup>().playerCanPickupSword == true && !playerHasSword)
        {
            pickupUI.SetActive(true);
        }
        else if (pickupTrigger.GetComponent<SwordPickup>().playerCanPickupSword == false && !playerHasSword)
        {
            pickupUI.SetActive(false);
        }

        //UI for End
        if (swordQuestStarted == true && endquestTrigger.GetComponent<EndQuestTrigger>().playerInEndTrigger == true && playerHasSword && !questComplete)
        {
            giveUI.SetActive(true);
            dontHaveSwordUI.SetActive(false);
            pressToTalkUI2.SetActive(false);

        }
        else if (swordQuestStarted == true && endquestTrigger.GetComponent<EndQuestTrigger>().playerInEndTrigger == true && !playerHasSword && !questComplete)
        {
            giveUI.SetActive(false);
            dontHaveSwordUI.SetActive(true);
            pressToTalkUI2.SetActive(true);
        }
        else if (endquestTrigger.GetComponent<EndQuestTrigger>().playerInEndTrigger == false)
        {
            giveUI.SetActive(false);
            dontHaveSwordUI.SetActive(false);
        }

        //Hide all UI when complete
        if (hasPlayerGivenSword == true)
        {
            pressToTalkUI2.SetActive(false);
            dontHaveSwordUI.SetActive(false);
            giveUI.SetActive(false);
            pickupUI.SetActive(false);
            
        }
    }
}
