using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        templeArea = GameObject.Find("TempleArea");
        hillArea = GameObject.Find("VillageHillArea");
        playerObject = GameObject.Find("ThirdPersonController");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(musicEventInstance, playerObject.transform, playerObject.GetComponent<Rigidbody>());
        musicEventInstance = FMODUnity.RuntimeManager.CreateInstance(gameMusic);
        musicEventInstance.start();

        ChangeParameter(globalMusicVolParam, currentMusicVolume);
    }

    // Update is called once per frame
    void Update()
    {
        CheckForQuestChange();
        ChangeGlobalVolumeMusic();
    }

    public void ChangeParameter(string parameter, float newValue)
    {
        musicEventInstance.setParameterByName(parameter, newValue);
    }

    void ChangeGlobalVolumeMusic()
    {
        if (Input.GetKeyDown(KeyCode.Minus) && currentMusicVolume > 0f)
        {
            currentMusicVolume -= 0.1f;
            ChangeParameter(globalMusicVolParam, currentMusicVolume);
        }
        else if (Input.GetKeyDown(KeyCode.Equals) && currentMusicVolume < 1f)
        {
            currentMusicVolume += 0.1f;
            ChangeParameter(globalMusicVolParam, currentMusicVolume);
        }
    }


    void CheckForQuestChange()
    {
        if (leekQuest.GetComponent<TheLeekQuest>().questStarted == true && questCounter == 0)
        {
            //change to verse 1
            questCounter++;
            ChangeParameter(musicParams[0], 0f);
            ChangeParameter(musicParams[1], 1f);
        }
        if (gongQuest.GetComponent<GongQuest>().questStarted == true && questCounter == 1)
        {
            //change to verse 2
            questCounter++;
            ChangeParameter(musicParams[1], 0f);
            ChangeParameter(musicParams[2], 1f);
        }
        if (swordQuest.GetComponent<QuestLogic>().swordQuestStarted == true && questCounter == 2)
        {
            //change to verse 3
            questCounter++;
            ChangeParameter(musicParams[2], 0f);
            ChangeParameter(musicParams[3], 1f);

        }
        if (swordQuest.GetComponent<QuestLogic>().questComplete == true && questCounter == 3)
        {
            //change to end theme
            ChangeParameter(musicParams[3], 0f);
            ChangeParameter(musicParams[4], 1f);
        }

        if (questCounter == 3)
        {
            if (swordQuest.GetComponent<QuestLogic>().playerHasSword)
            {
                ChangeParameter(instrumentParams[2], 1f);
                if (hillArea.GetComponent<NewAreaDiscovered>().inTrigger)
                {
                    ChangeParameter(instrumentParams[0], 1f);
                }

            }
            if (templeArea.GetComponent<NewAreaDiscovered>().inTrigger)
            {
                ChangeParameter(instrumentParams[1], 1f);
            }
            if (swordQuest.GetComponent<QuestLogic>().hasPlayerGivenSword)
            {
                ChangeParameter(instrumentParams[3], 1f);
            }
        }

    }

    GameObject playerObject;

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string gameMusic = "";
    FMOD.Studio.EventInstance musicEventInstance;
    string[] musicParams = { "ToIntro", "ToVerse1", "ToVerse2", "ToTheme1", "ToEnd" };
    string[] instrumentParams = { "StringTrillMusicVol", "DaikoMusicVol", "Theme1MusicVol", "Theme2MusicVol" };
    string globalMusicVolParam = "GlobalMusicVol";

    public float currentMusicVolume = 0.5f;
    int questCounter = 0;

    [Header("Quests")]
    public GameObject swordQuest;
    public GameObject leekQuest;
    public GameObject gongQuest;
    GameObject templeArea;
    GameObject hillArea;
}
